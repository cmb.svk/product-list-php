<?php

class Database {
  protected $mysqli;
  protected $data;

  public function __construct() {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $this->insert();
    }
  }

  private function connect() {
    $mysql = new mysqli("database", "root", "tiger", "docker");

    if ($mysql->connect_error) {
      die("Connection failed: " . $mysql->connect_error);
    }

    $this->mysqli = $mysql;
  }

  private function validateData() {
    $this->data = $_REQUEST;

    if (empty($this->data)) {
      die('empty request');
    }

    if (!array_key_exists('name', $this->data)) {
      die('nope. key "name" does not exist');
    }

    if (!array_key_exists('size', $this->data)) {
      die('nope. key "size" does not exist');
    }

    if (empty($this->data['name'])) {
      die('name is empty');
    }

    if (empty($this->data['size'])) {
      die('size is empty');
    }

    return true;
  }


  private function insert() {
    $this->validateData();
    $this->connect();

    $sql = "INSERT INTO products VALUES (NULL, '".$this->data['name']."', '".$this->data['size']."')";

    if(!$this->mysqli->query($sql)) {
      die("ERROR: Hush! Sorry " . $this->mysqli->error);
    }

   $this->mysqli->close();
   $this->mysqli = NULL;

   header('Location: product-list.php'); // redirect to product-list.php
   exit();
  }

  public function getProducts() {
    $this->connect();

    $sql = "SELECT id, name, size FROM products";
    $result = $this->mysqli->query($sql);

    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["name"]. " - Size: " . $row["size"]. "<br>";
      }
    } else {
      echo "0 results";
    }

    $this->mysqli->close();
    $this->mysqli = NULL;
  }
}

$db = new Database();